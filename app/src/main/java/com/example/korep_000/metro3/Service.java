package com.example.korep_000.metro3;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

/**
 * Created by korep_000 on 31.05.2016.
 */
public class Service extends android.app.Service {

    NotificationManager nm;
    Notification notification;
    String server = "http://10.2.13.53:8023/metro_location";
    String text_from_server = "";
    String file_name = "data_location.txt";
    String all = "all.txt";

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("service", "Service is launched");
        checkLocation();
        return android.app.Service.START_STICKY;
    }
    void checkLocation(){
        new Thread(new Runnable() {
            public void run() {
                TelephonyManager telephonyManager = (TelephonyManager)
                        getSystemService(android.content.Context.TELEPHONY_SERVICE);
                if (telephonyManager.getCellLocation() instanceof GsmCellLocation) {
                    GsmCellLocation gsmCell = (GsmCellLocation) telephonyManager.getCellLocation();
                    String cid = Integer.toString(gsmCell.getCid());
                    String lac = Integer.toString(gsmCell.getLac());
                    String operator = telephonyManager.getNetworkOperator();
                    String mcc = operator.substring(0, 3);
                    String mnc = operator.substring(3);
                    String id = "id:" + mcc + ":" + mnc + ":" + lac + ":" + cid;
                    readFromServer();
                    String text = text_from_server;
                    Log.d("from server  ", text);
                    try {
                        int max = 0;
                        String coord = "";
                        URL oracle = new URL(server + "/" + all);
                        URLConnection yc = oracle.openConnection();
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                yc.getInputStream()));
                        String inputLine;
                        while ((inputLine = in.readLine()) != null) {
                            int i = inputLine.indexOf(id);
                            if (i != -1) {
                                changeString(inputLine);
                                String[] parts = inputLine.split(",");
                                for (int j = 0; j < parts.length; ++j) {
                                    if (parts[j] != "") {
                                        String[] substring = parts[j].split("-");
                                        if (Integer.parseInt(substring[1]) > max) {
                                            max = Integer.parseInt(substring[1]);
                                            coord = substring[0];
                                        } else if (Integer.parseInt(substring[1]) == max) {
                                            Random n = new Random();
                                            int nn = n.nextInt(1);
                                            if (nn == 1) {
                                                max = Integer.parseInt(substring[1]);
                                                coord = substring[0];
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        }
                        in.close();
                        Log.d("coords first:  ", coord);
                        if (coord != "") {
                            String[] c = coord.split(":");
                            String[] cc = c[1].split("y");
                            sendNotif();
                            //painting(Float.parseFloat(cc[0]), Float.parseFloat(c[2]));
                        } else{
                            Log.d("Server", "There is no connection");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                stopSelf();
            }
        }).start();
    }
    void sendNotif() {
        Notification.Builder builder = new Notification.Builder(Service.this);

        builder.setAutoCancel(false);
        builder.setTicker("this is ticker text");
        builder.setContentTitle("WhatsApp Notification");
        builder.setContentText("Wake up!");
        builder.setSmallIcon(R.drawable.red_circle);
        //builder.setContentIntent(pIntent);
        builder.setOngoing(true);  //API level 16
        builder.setNumber(100);
        builder.build();

        notification = builder.getNotification();
        nm.notify(11, notification);
    }
    private void readFromServer() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    // Create a URL for the desired page
                    String url = server + "/" + file_name;
                    // Read all the text returned by the server
                    //BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                    text_from_server = readUrl(url);
                    Log.d("String/  ", text_from_server);
            /*StringBuffer stringBuffer = new StringBuffer();
            while((str = in.readLine()) != null){
                stringBuffer.append(str + "\n");
                Log.d("String/  ", str);
            }
            String out = stringBuffer.toString();
            Log.d("String/  ", out);
            in.close();*/
                } catch (MalformedURLException e) {
                    Log.d("eee:   ", "MalformedURLException");
                } catch (IOException e) {
                    Log.d("eee:   ", "IOException");
                } catch (java.lang.Exception e) {
                    Log.d("eee:   ", "java.lang.Exception");
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private String changeString(String str) {
        int i = str.indexOf("_");
        ++i;
        String new_str = "";
        while (i != str.length()) {
            new_str += str.indexOf(i);
            i += 1;
        }
        str = new_str;
        return str;
    }
    private String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);
            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
