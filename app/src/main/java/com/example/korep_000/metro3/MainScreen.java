package com.example.korep_000.metro3;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MotionEvent;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.korep_000.metro3.R;

import org.apache.http.HttpResponse;
import org.apache.http.entity.InputStreamEntity;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.Random;

public class MainScreen extends AppCompatActivity implements OnTouchListener {

    TextView text;
    ImageView imageView;
    ImageView redCircle;

    static float cir_x = -200000;
    static float cir_y = -200000;

    AlarmManager am;
    NotificationManager nm;
    Notification notification;
    Intent intent;
    PendingIntent pIntent;

    String station_now = "";
    String station_now_coor = "";
    String server = "http://mgust.ru:8023/metro_location/";
    String text_from_server = "";
    String file_name = "data_location.txt";
    String all = "all.txt";

    public static final int MENU_PROSPEKTMIRA_ORANGE = 1;
    public static final int MENU_PROSPEKTMIRA_BROWN = 2;
    public static final int MENU_CHISTYE_PRUDY = 3;
    public static final int MENU_TURGENEVSKAYA = 4;
    public static final int MENU_SRETENSKY_BULVAR = 5;
    public static final int MENU_KITAY_GOROD_PURPLE = 6;
    public static final int MENU_KITAY_GOROD_ORANGE = 7;
    public static final int MENU_TRETYAKOVSKAYA_YELLOW = 8;
    public static final int MENU_TRETYAKOVSKAYA_ORANE = 9;
    public static final int MENU_NOVOKUZNETSKAYA = 10;
    public static final int MENU_OKTYABRSKAYA_BROWN = 11;
    public static final int MENU_OKTABRSKAYA_ORANGE = 12;
    public static final int MENU_NOVOYASENEVSKAYA = 13;
    public static final int MENU_BITSEVSKY_PARK = 14;
    public static final int MENU_BULVAR_DMITRIYA_DONSKOGO = 15;
    public static final int MENU_ULITSA_STAROKACHALOVA = 16;
    public static final int MENU_KAKHOVSKAYA = 17;
    public static final int MENU_SEVASTOPOLSKAYA = 18;
    public static final int MENU_DOBRYNINSKAYA = 19;
    public static final int MENU_SERPUKHOVSKAYA = 20;
    public static final int MENU_BIBLIOTEKA_IMENI_LENINA = 21;
    public static final int MENU_ALEKSANDROVSKY_SAD = 22;
    public static final int MENU_ARBATSKAYA = 23;
    public static final int MENU_BOROVITSKAYA = 24;
    public static final int MENU_PUSHKINSKAYA = 25;
    public static final int MENU_CHEKHOVSKAYA = 26;
    public static final int MENU_TVERSKAYA = 27;
    public static final int MENU_TRUBNAYA = 28;
    public static final int MENU_TSVETNOY_BULVAR = 29;
    public static final int MENU_MENDELEEVSKAYA = 30;
    public static final int MENU_NOVOSLOBODSKAYA = 31;
    public static final int MENU_LUBYANKA = 32;
    public static final int MENU_KUZNETSKY_MOST = 33;
    public static final int MENU_OKHOTNY_RYAD = 34;
    public static final int MENU_TEATRALNAYA = 35;
    public static final int MENU_PLOSHCHAD_REVOLUTSII = 36;
    public static final int MENU_KOMSOMOLSKAYA_RED = 37;
    public static final int MENU_KOMSOMOLSKAYA_BROWN = 38;
    public static final int MENU_KURSKAYA_BROWN = 39;
    public static final int MENU_KURSKAYA_BLUE = 40;
    public static final int MENU_CHKALOVSKAYA = 41;
    public static final int MENU_TAGANSKAYA_PURPLE = 42;
    public static final int MENU_TAGANSKAYA_BROWN = 43;
    public static final int MENU_MARKSISTSKAYA = 44;
    public static final int MENU_PAVELETSKAYA_BROWN = 45;
    public static final int MENU_PAVELETSKAYA_GREEN = 46;
    public static final int MENU_KASHIRSKAYA_GREEN = 47;
    public static final int MENU_KASHIRSKAYA_BLUE_GREY = 48;
    public static final int MENU_ZYABLOKOVO = 49;
    public static final int MENU_KRASNOGVARDEYSKAYA = 50;
    public static final int MENU_PROLETARSKAYA = 51;
    public static final int MENU_KRESTYANSKAYA_ZASTAVA = 52;
    public static final int MENU_RIMSKAYA = 53;
    public static final int MENU_PLOSHCHAD_ILICHA = 54;
    public static final int MENU_BELORUSSKAYA_GREEN = 55;
    public static final int MENU_BELORUSSKAYA_BROWN = 56;
    public static final int MENU_KRASNOPRESNENSKAYA = 57;
    public static final int MENU_BARRIKADNAYA = 58;
    public static final int MENU_KIYEVSKAYA_BROWN = 59;
    public static final int MENU_KIYEVSKAYA_BLUE = 60;
    public static final int MENU_KIYEVSKAYA_SKY = 61;
    public static final int MENU_KUNTSEVSKAYA_BLUE = 62;
    public static final int MENU_KUNTSEVSKAYA_SKY = 63;
    public static final int MENU_PARK_POBEDY_YELLOW = 64;
    public static final int MENU_PARK_POBEDY_BLUE = 65;
    public static final int MENU_VYSTAVOCHNAYA = 66;
    public static final int MENU_DELOVOY_TSENTR = 67;
    public static final int MENU_PARK_KULTURY_BROWN = 68;
    public static final int MENU_PARK_KULTURY_RED = 69;
    public static final int MENU_VARSHAVSKAYA = 70;
    public static final int MENU_POLEZHAEVSKAYA = 71;
    public static final int MENU_DINAMO = 72;

    private float starting_x = 0;
    private float starting_y = 0;

    private static final String TAG = "Touch";
    @SuppressWarnings("unused")
    private static final float MIN_ZOOM = 1f, MAX_ZOOM = 1f;

    Matrix matrix;
    Matrix savedMatrix = new Matrix();

    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;

    public MainScreen() {
        matrix = new Matrix();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions(MainScreen.this,
                    new String[] {
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    100);
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions(MainScreen.this,
                    new String[] {
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    100);
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions(MainScreen.this,
                    new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION
                    },
                    100);
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions(MainScreen.this,
                    new String[] {
                            Manifest.permission.INTERNET
                    },
                    100);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button myButton = (Button) findViewById(R.id.button);
        myButton.setOnClickListener(station);
        text = (TextView) findViewById(R.id.textView);

        this.imageView = (ImageView) this.findViewById(R.id.imageView);
        imageView.setOnTouchListener(this);

        redCircle = (ImageView) this.findViewById(R.id.circle);
        redCircle.setVisibility(View.INVISIBLE);

        registerForContextMenu(text);
        /* For service`s working !!!!!!!!!!!!!!!!!!!
        intent = createIntent();
        pIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        am = (AlarmManager) getSystemService(ALARM_SERVICE);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 30000, 30000, pIntent);*/
        checkLocation();
    }
    // for alarmmanager
    Intent createIntent() {
        Intent intent = new Intent(this, Receiver.class);
        return intent;
    }
//check location and paint

    public void checkLocation() {
        new Thread(new Runnable() {
            public void run() {
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ){
                    ActivityCompat.requestPermissions(MainScreen.this,
                            new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION},
                            10);
                } else {
                    TelephonyManager telephonyManager = (TelephonyManager)
                            getSystemService(android.content.Context.TELEPHONY_SERVICE);
                    if (telephonyManager.getCellLocation() instanceof GsmCellLocation) {
                        GsmCellLocation gsmCell = (GsmCellLocation) telephonyManager.getCellLocation();
                        String cid = Integer.toString(gsmCell.getCid());
                        String lac = Integer.toString(gsmCell.getLac());
                        String operator = telephonyManager.getNetworkOperator();
                        String mcc = operator.substring(0, 3);
                        String mnc = operator.substring(3);

                        String id = "id:" + mcc + ":" + mnc + ":" + lac + ":" + cid;
                        Log.d("id  ", id);
                        //readFromServer();
                        //String text = readFileSD();
                        try {
                            int max = 0;
                            String coord = "";
                            URL oracle = new URL(server + all);
                            URLConnection yc = oracle.openConnection();
                            BufferedReader in = new BufferedReader(new InputStreamReader(
                                    yc.getInputStream()));
                            String inputLine;
                            while((inputLine = in.readLine()) != null) {
                                Log.d("inputLine ", inputLine);
                                int i = inputLine.indexOf(id);
                                if (i != -1) {
                                    changeString(inputLine);
                                    String[] parts = inputLine.split(",");
                                    for (int j = 0; j < parts.length; ++j) {
                                        if (parts[j] != "") {
                                            String[] substring = parts[j].split("-");
                                            if (Integer.parseInt(substring[1]) > max) {
                                                max = Integer.parseInt(substring[1]);
                                                coord = substring[0];
                                            } else if (Integer.parseInt(substring[1]) == max) {
                                                Random n = new Random();
                                                int nn = n.nextInt(1);
                                                if (nn == 1) {
                                                    max = Integer.parseInt(substring[1]);
                                                    coord = substring[0];
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                            in.close();
                            Log.d("coords first:  ", coord);
                            if (coord != "") {
                                String[] c = coord.split(":");
                                String[] cc = c[1].split("y");
                                cir_x = Float.parseFloat(cc[0]);
                                cir_y = Float.parseFloat(c[2]);
                                Log.d("cir_x, y  ", cc[0] + " " + c[2]);

                            } else{
                                Log.d("location:  ", "Location is not defined!");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("url", "There is no connection");
                        }
                    }
                }
            }
        }).start();
    }
//for previous function
    private String changeString(String str) {
        int i = str.indexOf("_");
        ++i;
        String new_str = "";
        while (i != str.length()) {
            new_str += str.indexOf(i);
            i += 1;
        }
        str = new_str;
        return str;
    }
    /*private boolean isConnectedToServer(String url, int timeout) {
        try{
            URL myUrl = new URL(url);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(timeout);
            connection.connect();
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/

//for button
    OnClickListener station = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //File file = new File(getFilesDir(), "data_location.txt");
            //file.delete();
            /*if (file.exists()) {
                Log.v(TAG, "File exists! " + file.getAbsolutePath());
                String out = readFile(file_name);
                Log.v(TAG, out + file.getAbsolutePath());
                Toast toast = Toast.makeText(getApplicationContext(),
                        "File exists! ", Toast.LENGTH_SHORT);
                toast.show();
                Toast toast1 = Toast.makeText(getApplicationContext(),
                        out, Toast.LENGTH_SHORT);
                toast1.show();
            } else {
                Log.v(TAG, "File NOT exists! " + file.getAbsolutePath());
                writeFile("data_location.txt", "smth else");
                Toast toast = Toast.makeText(getApplicationContext(),
                        "File NOT exists! ", Toast.LENGTH_SHORT);
                toast.show();
            }*/
           // painting(1119, 906);

            if (station_now != "") {
                //File file = new File(getFilesDir(), "data_location.txt");
                //file.delete();
                onCellLocationChanged(file_name);
                sendToServer();
                //readFromServer();
                //painting(1118, 1069);
                checkLocation();
                if(cir_x != -200000 && cir_y != -200000){
                    painting(cir_x, cir_y);
                }
                station_now = "";
                station_now_coor = "";
                text.setText("");
            }
        }
    };

//mark your location
    private void painting(float x, float y){
        Log.d("x, y  ", Float.toString(x) + " " + Float.toString(y));
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Bitmap backgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.map_sbw);
        Bitmap bitmapToDrawInTheCenter = BitmapFactory.decodeResource(getResources(), R.drawable.red_circle);

        Bitmap resultBitmap = Bitmap.createBitmap(backgroundBitmap.getWidth(),backgroundBitmap.getHeight(), backgroundBitmap.getConfig());
        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(backgroundBitmap, new Matrix(), null);
        float[] value = new float[9];
        //value = imageCoord(redCircle);
        Log.d("size  ", Float.toString(redCircle.getWidth()) + " " + Float.toString(redCircle.getHeight()));
        canvas.drawBitmap(bitmapToDrawInTheCenter, x - 64 ,
                y - 64, paint);

        ImageView image = (ImageView)findViewById(R.id.imageView);
        image.setImageBitmap(resultBitmap);
        Log.d("painting:  ", "ok");
    }
//read all.txt from /metro_location/all.txt
    private void readFromServer() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    // Create a URL for the desired page
                    String url = server + all;
                    // Read all the text returned by the server
                    //BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                    text_from_server = readUrl(url);
                    Log.d("String/  ", text_from_server);
            /*StringBuffer stringBuffer = new StringBuffer();
            while((str = in.readLine()) != null){
                stringBuffer.append(str + "\n");
                Log.d("String/  ", str);
            }
            String out = stringBuffer.toString();
            Log.d("String/  ", out);
            in.close();*/
                } catch (MalformedURLException e) {
                    Log.d("eee:   ", "MalformedURLException");
                } catch (IOException e) {
                    Log.d("eee:   ", "IOException");
                } catch (java.lang.Exception e) {
                    Log.d("eee:   ", "java.lang.Exception");
                    e.printStackTrace();
                }
            }
        }).start();
    }
//send to /metro_location/
    public void sendToServer() {
        new Thread(new Runnable() {
            public void run() {
                String url = server;
                String charset = "UTF-8";
                File sdPath = Environment.getExternalStorageDirectory();
                sdPath = new File(sdPath.getAbsolutePath() + "/" + file_name);
                sdPath.mkdirs();
                File textFile;
                if (!Environment.getExternalStorageState().equals(
                        Environment.MEDIA_MOUNTED)) {
                    textFile = new File(getFilesDir(), file_name);
                } else {
                    textFile = new File(sdPath, file_name);
                }
                if(textFile.exists()){
                    String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
                    String CRLF = "\r\n"; // Line separator required by multipart/form-data.
                    System.setProperty("http.keepAlive", "false");
                    try {
                        URLConnection connection = new URL(url).openConnection();
                        connection.setDoOutput(true);
                        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                        OutputStream output = connection.getOutputStream();
                        PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
                        // Send text file.
                        writer.append("--" + boundary).append(CRLF);
                        writer.append("Content-Disposition: form-data; name=\"textFile\"; filename=\""
                                + textFile.getName() + "\"").append(CRLF);
                        writer.append("Content-Type: text/plain; charset="
                                + charset).append(CRLF); // Text file itself must be saved in this charset!
                        writer.append(CRLF).flush();
                        BufferedReader reader = null;
                        try {
                            reader = new BufferedReader(new InputStreamReader(new FileInputStream(textFile), "UTF-8"));
                            for (String line; (line = reader.readLine()) != null; ) {
                                writer.println(line);
                            }
                        } finally {
                            if (reader != null) try {
                                reader.close();
                            } catch (IOException logOrIgnore) {
                            }
                        }

                        output.flush(); // Important before continuing with writer!
                        writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.
                        output.close();

                        // End of multipart/form-data.
                        writer.append("--" + boundary + "--").append(CRLF).flush();

                        // Request is lazily fired whenever you need to obtain information about response.
                        int responseCode = ((HttpURLConnection) connection).getResponseCode();
                        Log.d("responseCode/  ", Integer.toString(responseCode));// Should be 200
                        if (responseCode == 200) {
                            textFile.delete();
                        }
                    } catch (MalformedURLException e) {
                        Log.d("eee:  ", "MalformedURLException");
                    } catch (IOException e) {
                        Log.d("eee:  ", "IOException");
                    }
                }
            }
        }).start();
    }

//multitouch: zoom, scrolling
    private long startTime = 0;
    private long endTime = 0;

    public boolean onTouch(View v, MotionEvent event) {

        ImageView view = (ImageView) v;
        view.setScaleType(ImageView.ScaleType.MATRIX);
        float scale;
        float[] values = new float[9];

        //dumpEvent(event);

        String st = "";
        text.setText("");

        Float startCircle_x = redCircle.getX();//matrix for redCircle
        Float startCircle_y = redCircle.getY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                starting_x = event.getX();
                starting_y = event.getY();
                station_now = "";
                matrix.set(view.getImageMatrix());
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                Log.d(TAG, "mode=DRAG");
                mode = DRAG;
                break;
            case MotionEvent.ACTION_UP:
                endTime = event.getEventTime();
                break;
            case MotionEvent.ACTION_POINTER_UP: // second finger lifted
                mode = NONE;
                Log.d(TAG, "mode=NONE");
                break;
            case MotionEvent.ACTION_POINTER_DOWN: // first and second finger down
                oldDist = spacing(event);
                Log.d(TAG, "oldDist=" + oldDist);
                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                    Log.d(TAG, "mode=ZOOM");
                }
                break;
            case MotionEvent.ACTION_MOVE:
                station_now = "";
                station_now_coor = "";
                endTime = 0;
                float x = event.getX();
                float y = event.getY();
                starting_x = x;
                starting_y = y;
                values = imageCoord(view);
                if (mode == DRAG) {
                    matrix.set(savedMatrix);
                    float dx = x - start.x;
                    float dy = y - start.y;
                    matrix.postTranslate(dx, dy);// create the transformation in the matrix  of point
                } else if (mode == ZOOM) {
                    float newDist = spacing(event);
                    Log.d(TAG, "newDist=" + newDist);
                    if (newDist > 5f) {
                        matrix.set(savedMatrix);
                        scale = newDist / oldDist;
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                }
                break;
        }
        values = imageCoord(view);
        view.setImageMatrix(matrix);
        int num = 20 * 20;
        float currX = (starting_x - values[Matrix.MTRANS_X]) / values[Matrix.MSCALE_X];
        float currY = (starting_y - values[Matrix.MTRANS_Y]) / values[Matrix.MSCALE_Y];
        Boolean flag = false;
        /*float[] value_circle = new float[9];
        value_circle = imageCoord(redCircle);
        Log.d("scale image", Float.toString(startCircle_x) + " " +
                Float.toString(startCircle_y));

        int sign_x = 1;
        int sign_y = 1;
        if(starting_x - start.x < 0){
            sign_x = -1;
        }
        if(starting_y - start.y < 0){
            sign_y = -1;
        }
        redCircle.setX(startCircle_x + sign_x * Math.abs(starting_x - start.x) * value_circle[Matrix.MSCALE_X]);
        redCircle.setY(startCircle_y + sign_y * Math.abs(starting_y - start.y) * value_circle[Matrix.MSCALE_Y]); //(starting_y - start.y) - make modul
        Log.d("due to image", Float.toString((startCircle_x + sign_x *
                Math.abs(starting_x - start.x) * value_circle[Matrix.MSCALE_X]
                - values[Matrix.MTRANS_X]) / values[Matrix.MSCALE_X]) + " " +
                Float.toString((startCircle_y + sign_y * Math.abs(starting_y - start.y)
                        * value_circle[Matrix.MSCALE_Y] - values[Matrix.MTRANS_Y]) / values[Matrix.MSCALE_Y]));
        */

        if (endTime - startTime >= 5000) {
            if (Math.pow(1280 - currX, 2) + Math.pow(570 - currY, 2) <= num) {
                station_now = "Prospekt Mira";
                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(570);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(200 - currY, 2) <= num) {
                station_now = "Medvedkovo";
                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(200);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(240 - currY, 2) <= num) {
                station_now = "Babushkinskaya";
                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(240);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(279 - currY, 2) <= num) {
                station_now = "Sviblovo";
                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(279);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(320 - currY, 2) <= num) {
                station_now = "Botanichesky sad";

                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(320);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(420 - currY, 2) <= num) {
                station_now = "VDNKh";

                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(420);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(459 - currY, 2) <= num) {
                station_now = "Alekseevskaya";

                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(459);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(500 - currY, 2) <= num) {
                station_now = "Rizhskaya";

                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(500);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(680 - currY, 2) <= num) {
                station_now = "Sukharevskaya";

                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(680);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(745 - currY, 2) <= num) {
                station_now = "Chistiye Prudy, Turgenevskaya, Sretensky Bulvar";

                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(745);
                flag = true;
            } else if (Math.pow(1280 - currX, 2) + Math.pow(906 - currY, 2) <= num) {
                station_now = "Kitay - Gorod";

                station_now_coor = " x:"
                        + Integer.toString(1280) + "y:"
                        + Integer.toString(906);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1306 - currY, 2) <= num) {
                station_now = "Tretyakovskaya, Novokuznetskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1306);
                flag = true;
            } else if (Math.pow(948 - currX, 2) + Math.pow(1475 - currY, 2) <= num) {
                station_now = "Oktyabrskaya";

                station_now_coor = " x:"
                        + Integer.toString(948) + "y:"
                        + Integer.toString(1475);
                flag = true;
            } else if (Math.pow(907 - currX, 2) + Math.pow(1516 - currY, 2) <= num) {
                station_now = "Shabolovskaya";

                station_now_coor = " x:"
                        + Integer.toString(907) + "y:"
                        + Integer.toString(1516);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1579 - currY, 2) <= num) {
                station_now = "Leninsky Prospekt";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1579);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1619 - currY, 2) <= num) {
                station_now = "Akademicheskaya";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1619);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1659 - currY, 2) <= num) {
                station_now = "Profsoyuznaya";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1659);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1698 - currY, 2) <= num) {
                station_now = "Novye Cheremushki";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1698);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1740 - currY, 2) <= num) {
                station_now = "Kaluzhskaya";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1740);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1779 - currY, 2) <= num) {
                station_now = "Belyaevo";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1779);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1819 - currY, 2) <= num) {
                station_now = "konkovo";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1819);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1860 - currY, 2) <= num) {
                station_now = "Tyoply Stan";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1860);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1899 - currY, 2) <= num) {
                station_now = "Yasenevo";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1899);
                flag = true;
            } else if (Math.pow(881 - currX, 2) + Math.pow(1943 - currY, 2) <= num) {
                station_now = "Bitsevsky Park, Novoyasenevskaya";

                station_now_coor = " x:"
                        + Integer.toString(881) + "y:"
                        + Integer.toString(1943);
                flag = true;
            } else if (Math.pow(1026 - currX, 2) + Math.pow(2055 - currY, 2) <= num) {
                station_now = "Lesoparkovaya";

                station_now_coor = " x:"
                        + Integer.toString(1026) + "y:"
                        + Integer.toString(2055);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(2229 - currY, 2) <= num) {
                station_now = "Buninskaya Alleya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(2229);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(2189 - currY, 2) <= num) {
                station_now = "Ulitsa Gorchakova";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(2189);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(2148 - currY, 2) <= num) {
                station_now = "Bulvar Admirala Ushakova";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(2148);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(2109 - currY, 2) <= num) {
                station_now = "Ulitsa Skobelevskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(2109);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(2052 - currY, 2) <= num) {
                station_now = "Bulvar Dmitriya Donskogo, Ulitsa Starokachalovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(2052);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1998 - currY, 2) <= num) {
                station_now = "Annino";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1998);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1959 - currY, 2) <= num) {
                station_now = "Ulitsa Akademika Yangelya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1959);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1918 - currY, 2) <= num) {
                station_now = "Prazhskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1918);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1880 - currY, 2) <= num) {
                station_now = "Yuzhnaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1880);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1839 - currY, 2) <= num) {
                station_now = "Chertanovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1839);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1748 - currY, 2) <= num) {
                station_now = "Kakhovskaya, Sevastopolskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1748);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1719 - currY, 2) <= num) {
                station_now = "Nakhimovsky Prospekt";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1719);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1680 - currY, 2) <= num) {
                station_now = "Nagornaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1680);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1639 - currY, 2) <= num) {
                station_now = "Nagatinskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1639);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1598 - currY, 2) <= num) {
                station_now = "Tulskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1598);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1505 - currY, 2) <= num) {
                station_now = "Dobryninskaya, Serpukhovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1505);
                flag = true;
            } else if (Math.pow(952 - currX, 2) + Math.pow(1298 - currY, 2) <= num) {
                station_now = "Polyanka";

                station_now_coor = " x:"
                        + Integer.toString(952) + "y:"
                        + Integer.toString(1298);
                flag = true;
            } else if (Math.pow(877 - currX, 2) + Math.pow(1067 - currY, 2) <= num) {
                station_now = "Biblioteka Imeni Lenina, Aleksandrovsky Sad, Arbatskaya(A-P), Borovitskaya";

                station_now_coor = " x:"
                        + Integer.toString(877) + "y:"
                        + Integer.toString(1067);
                flag = true;
            } else if (Math.pow(960 - currX, 2) + Math.pow(907 - currY, 2) <= num) {
                station_now = "Pushkinskaya, Chekhovskaya, Tverskaya";

                station_now_coor = " x:"
                        + Integer.toString(960) + "y:"
                        + Integer.toString(907);
                flag = true;
            } else if (Math.pow(1120 - currX, 2) + Math.pow(744 - currY, 2) <= num) {
                station_now = "Trubnaya, Tsvetnoy Bulvar";

                station_now_coor = " x:"
                        + Integer.toString(1120) + "y:"
                        + Integer.toString(744);
                flag = true;
            } else if (Math.pow(880 - currX, 2) + Math.pow(609 - currY, 2) <= num) {
                station_now = "Mendeleevskaya, Novoslobodskaya";

                station_now_coor = " x:"
                        + Integer.toString(880) + "y:"
                        + Integer.toString(609);
                flag = true;
            } else if (Math.pow(880 - currX, 2) + Math.pow(498 - currY, 2) <= num) {
                station_now = "Savelovskaya";

                station_now_coor = " x:"
                        + Integer.toString(880) + "y:"
                        + Integer.toString(498);
                flag = true;
            } else if (Math.pow(880 - currX, 2) + Math.pow(459 - currY, 2) <= num) {
                station_now = "Dmitrovskaya";

                station_now_coor = " x:"
                        + Integer.toString(880) + "y:"
                        + Integer.toString(459);
                flag = true;
            } else if (Math.pow(880 - currX, 2) + Math.pow(419 - currY, 2) <= num) {
                station_now = "Timiryazevskaya";

                station_now_coor = " x:"
                        + Integer.toString(880) + "y:"
                        + Integer.toString(419);
                flag = true;
            } else if (Math.pow(1002 - currX, 2) + Math.pow(314 - currY, 2) <= num) {
                station_now = "Petrovsko - Razumovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1002) + "y:"
                        + Integer.toString(314);
                flag = true;
            } else if (Math.pow(1112 - currX, 2) + Math.pow(229 - currY, 2) <= num) {
                station_now = "Vladykino";

                station_now_coor = " x:"
                        + Integer.toString(1112) + "y:"
                        + Integer.toString(229);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(204 - currY, 2) <= num) {
                station_now = "Otradnoye";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(204);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(164 - currY, 2) <= num) {
                station_now = "Bibirevo";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(164);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(125 - currY, 2) <= num) {
                station_now = "Altyfyevo";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(125);
                flag = true;
            } else if (Math.pow(1119 - currX, 2) + Math.pow(478 - currY, 2) <= num) {
                station_now = "Maryina Roshcha";

                station_now_coor = " x:"
                        + Integer.toString(1119) + "y:"
                        + Integer.toString(478);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(619 - currY, 2) <= num) {
                station_now = "Dostoevskaya";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(619);
                flag = true;
            } else if (Math.pow(1601 - currX, 2) + Math.pow(1605 - currY, 2) <= num) {
                station_now = "Kurskaya, Chkalovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1601) + "y:"
                        + Integer.toString(1605);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1307 - currY, 2) <= num) {
                station_now = "Rimskaya, Ploshchad Ilicha";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1307);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1460 - currY, 2) <= num) {
                station_now = "Krestyanskaya Zastava, Proletarskaya";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1460);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1532 - currY, 2) <= num) {
                station_now = "Dubrovka";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1532);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1574 - currY, 2) <= num) {
                station_now = "Kozhukhovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1574);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1611 - currY, 2) <= num) {
                station_now = "Pechatniki";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1611);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1653 - currY, 2) <= num) {
                station_now = "Volzhskaya";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1653);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1694 - currY, 2) <= num) {
                station_now = "Lyublino";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1694);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1733 - currY, 2) <= num) {
                station_now = "Bratislavskaya";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1733);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1773 - currY, 2) <= num) {
                station_now = "Maryino";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1773);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1813 - currY, 2) <= num) {
                station_now = "Borisovo";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1813);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1853 - currY, 2) <= num) {
                station_now = "Shipilovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1853);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1939 - currY, 2) <= num) {
                station_now = "Zyablikovo, Krasnogvardeyskaya";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1939);
                flag = true;
            } else if (Math.pow(1679 - currX, 2) + Math.pow(1995 - currY, 2) <= num) {
                station_now = "Alma - Atinskaya";

                station_now_coor = " x:"
                        + Integer.toString(1679) + "y:"
                        + Integer.toString(1995);
                flag = true;
            } else if (Math.pow(1439 - currX, 2) + Math.pow(1788 - currY, 2) <= num) {
                station_now = "Domodedovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1439) + "y:"
                        + Integer.toString(1788);
                flag = true;
            } else if (Math.pow(1439 - currX, 2) + Math.pow(1748 - currY, 2) <= num) {
                station_now = "Orekhovo";

                station_now_coor = " x:"
                        + Integer.toString(1439) + "y:"
                        + Integer.toString(1748);
                flag = true;
            } else if (Math.pow(1439 - currX, 2) + Math.pow(1708 - currY, 2) <= num) {
                station_now = "Tsaritsyno";

                station_now_coor = " x:"
                        + Integer.toString(1439) + "y:"
                        + Integer.toString(1708);
                flag = true;
            } else if (Math.pow(1439 - currX, 2) + Math.pow(1688 - currY, 2) <= num) {
                station_now = "Kantemirovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1439) + "y:"
                        + Integer.toString(1688);
                flag = true;
            } else if (Math.pow(1439 - currX, 2) + Math.pow(1628 - currY, 2) <= num) {
                station_now = "Kashirskaya";

                station_now_coor = " x:"
                        + Integer.toString(1439) + "y:"
                        + Integer.toString(1628);
                flag = true;
            } else if (Math.pow(1397 - currX, 2) + Math.pow(1628 - currY, 2) <= num) {
                station_now = "Varshavskaya";

                station_now_coor = " x:"
                        + Integer.toString(1397) + "y:"
                        + Integer.toString(1628);
                flag = true;
            } else if (Math.pow(1421 - currX, 2) + Math.pow(1605 - currY, 2) <= num) {
                station_now = "Kolomenskaya";

                station_now_coor = " x:"
                        + Integer.toString(1421) + "y:"
                        + Integer.toString(1605);
                flag = true;
            } else if (Math.pow(1383 - currX, 2) + Math.pow(1568 - currY, 2) <= num) {
                station_now = "Tekhnopark";

                station_now_coor = " x:"
                        + Integer.toString(1383) + "y:"
                        + Integer.toString(1568);
                flag = true;
            } else if (Math.pow(1349 - currX, 2) + Math.pow(1536 - currY, 2) <= num) {
                station_now = "Avtozavodskaya";

                station_now_coor = " x:"
                        + Integer.toString(1349) + "y:"
                        + Integer.toString(1536);
                flag = true;
            } else if (Math.pow(1284 - currX, 2) + Math.pow(1476 - currY, 2) <= num) {
                station_now = "Paveletskaya";

                station_now_coor = " x:"
                        + Integer.toString(1284) + "y:"
                        + Integer.toString(1476);
                flag = true;
            } else if (Math.pow(1118 - currX, 2) + Math.pow(1067 - currY, 2) <= num) {
                station_now = "Teatralnaya, Okhotny Ryad, Ploshchad Revolyutsii";

                station_now_coor = " x:"
                        + Integer.toString(1118) + "y:"
                        + Integer.toString(1067);
                flag = true;
            } else if (Math.pow(852 - currX, 2) + Math.pow(800 - currY, 2) <= num) {
                station_now = "Mayakovskaya";

                station_now_coor = " x:"
                        + Integer.toString(852) + "y:"
                        + Integer.toString(800);
                flag = true;
            } else if (Math.pow(760 - currX, 2) + Math.pow(708 - currY, 2) <= num) {
                station_now = "Belorusskaya";

                station_now_coor = " x:"
                        + Integer.toString(760) + "y:"
                        + Integer.toString(708);
                flag = true;
            } else if (Math.pow(638 - currX, 2) + Math.pow(456 - currY, 2) <= num) {
                station_now = "Aeroport";

                station_now_coor = " x:"
                        + Integer.toString(638) + "y:"
                        + Integer.toString(456);
                flag = true;
            } else if (Math.pow(637 - currX, 2) + Math.pow(414 - currY, 2) <= num) {
                station_now = "Sokol";

                station_now_coor = " x:"
                        + Integer.toString(637) + "y:"
                        + Integer.toString(414);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(376 - currY, 2) <= num) {
                station_now = "Voykovskaya";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(376);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(334 - currY, 2) <= num) {
                station_now = "Vodny Stadion";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(334);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(295 - currY, 2) <= num) {
                station_now = "Rechnoy Vokzal";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(295);
                flag = true;
            } else if (Math.pow(400 - currX, 2) + Math.pow(297 - currY, 2) <= num) {
                station_now = "Planernaya";

                station_now_coor = " x:"
                        + Integer.toString(400) + "y:"
                        + Integer.toString(297);
                flag = true;
            } else if (Math.pow(400 - currX, 2) + Math.pow(336 - currY, 2) <= num) {
                station_now = "Skhodnenskya";

                station_now_coor = " x:"
                        + Integer.toString(400) + "y:"
                        + Integer.toString(336);
                flag = true;
            } else if (Math.pow(400 - currX, 2) + Math.pow(375 - currY, 2) <= num) {
                station_now = "Tushinskaya";

                station_now_coor = " x:"
                        + Integer.toString(400) + "y:"
                        + Integer.toString(375);
                flag = true;
            } else if (Math.pow(400 - currX, 2) + Math.pow(414 - currY, 2) <= num) {
                station_now = "Spartak";

                station_now_coor = " x:"
                        + Integer.toString(400) + "y:"
                        + Integer.toString(414);
                flag = true;
            } else if (Math.pow(400 - currX, 2) + Math.pow(455 - currY, 2) <= num) {
                station_now = "Shchukinskaya";

                station_now_coor = " x:"
                        + Integer.toString(400) + "y:"
                        + Integer.toString(455);
                flag = true;
            } else if (Math.pow(400 - currX, 2) + Math.pow(495 - currY, 2) <= num) {
                station_now = "Oktyabrskoe Pole";

                station_now_coor = " x:"
                        + Integer.toString(400) + "y:"
                        + Integer.toString(495);
                flag = true;
            } else if (Math.pow(489 - currX, 2) + Math.pow(757 - currY, 2) <= num) {
                station_now = "Begovaya";

                station_now_coor = " x:"
                        + Integer.toString(489) + "y:"
                        + Integer.toString(757);
                flag = true;
            } else if (Math.pow(535 - currX, 2) + Math.pow(801 - currY, 2) <= num) {
                station_now = "Ulitsa 1905 goda";

                station_now_coor = " x:"
                        + Integer.toString(535) + "y:"
                        + Integer.toString(801);
                flag = true;
            } else if (Math.pow(655 - currX, 2) + Math.pow(905 - currY, 2) <= num) {
                station_now = "Krasnopresnenskaya, Barrikadnaya";

                station_now_coor = " x:"
                        + Integer.toString(655) + "y:"
                        + Integer.toString(905);
                flag = true;
            } else if (Math.pow(1120 - currX, 2) + Math.pow(905 - currY, 2) <= num) {
                station_now = "Kuznetsky Most, Lubyanka";

                station_now_coor = " x:"
                        + Integer.toString(1120) + "y:"
                        + Integer.toString(905);
                flag = true;
            } else if (Math.pow(1509 - currX, 2) + Math.pow(1306 - currY, 2) <= num) {
                station_now = "Marksistskaya, Taganskaya";

                station_now_coor = " x:"
                        + Integer.toString(1509) + "y:"
                        + Integer.toString(1306);
                flag = true;
            } else if (Math.pow(1835 - currX, 2) + Math.pow(1498 - currY, 2) <= num) {
                station_now = "Volgogradsky Prospekt";

                station_now_coor = " x:"
                        + Integer.toString(1835) + "y:"
                        + Integer.toString(1498);
                flag = true;
            } else if (Math.pow(1877 - currX, 2) + Math.pow(1550 - currY, 2) <= num) {
                station_now = "Tekstilshchiki";

                station_now_coor = " x:"
                        + Integer.toString(1877) + "y:"
                        + Integer.toString(1550);
                flag = true;
            } else if (Math.pow(1877 - currX, 2) + Math.pow(1589 - currY, 2) <= num) {
                station_now = "Kuzminki";

                station_now_coor = " x:"
                        + Integer.toString(1877) + "y:"
                        + Integer.toString(1589);
                flag = true;
            } else if (Math.pow(1877 - currX, 2) + Math.pow(1627 - currY, 2) <= num) {
                station_now = "Ryazansky Prospekt";

                station_now_coor = " x:"
                        + Integer.toString(1877) + "y:"
                        + Integer.toString(1627);
                flag = true;
            } else if (Math.pow(1877 - currX, 2) + Math.pow(1688 - currY, 2) <= num) {
                station_now = "Vykhino";

                station_now_coor = " x:"
                        + Integer.toString(1877) + "y:"
                        + Integer.toString(1688);
                flag = true;
            } else if (Math.pow(1877 - currX, 2) + Math.pow(1728 - currY, 2) <= num) {
                station_now = "Lermontovsky Prospekt";

                station_now_coor = " x:"
                        + Integer.toString(1877) + "y:"
                        + Integer.toString(1728);
                flag = true;
            } else if (Math.pow(1877 - currX, 2) + Math.pow(1789 - currY, 2) <= num) {
                station_now = "Zhulebino";

                station_now_coor = " x:"
                        + Integer.toString(1877) + "y:"
                        + Integer.toString(1789);
                flag = true;
            } else if (Math.pow(1877 - currX, 2) + Math.pow(1830 - currY, 2) <= num) {
                station_now = "Kotelniki";

                station_now_coor = " x:"
                        + Integer.toString(1877) + "y:"
                        + Integer.toString(1830);
                flag = true;
            } else if (Math.pow(1790 - currX, 2) + Math.pow(1195 - currY, 2) <= num) {
                station_now = "Aviamotornaya";

                station_now_coor = " x:"
                        + Integer.toString(1790) + "y:"
                        + Integer.toString(1195);
                flag = true;
            } else if (Math.pow(1826 - currX, 2) + Math.pow(1159 - currY, 2) <= num) {
                station_now = "Shosse Entuziastov";

                station_now_coor = " x:"
                        + Integer.toString(1826) + "y:"
                        + Integer.toString(1159);
                flag = true;
            } else if (Math.pow(1836 - currX, 2) + Math.pow(1138 - currY, 2) <= num) {
                station_now = "Perovo";

                station_now_coor = " x:"
                        + Integer.toString(1836) + "y:"
                        + Integer.toString(1138);
                flag = true;
            } else if (Math.pow(1839 - currX, 2) + Math.pow(1100 - currY, 2) <= num) {
                station_now = "Novogireevo";

                station_now_coor = " x:"
                        + Integer.toString(1839) + "y:"
                        + Integer.toString(1100);
                flag = true;
            } else if (Math.pow(1839 - currX, 2) + Math.pow(1059 - currY, 2) <= num) {
                station_now = "Novokosino";

                station_now_coor = " x:"
                        + Integer.toString(1839) + "y:"
                        + Integer.toString(1059);
                flag = true;
            } else if (Math.pow(1840 - currX, 2) + Math.pow(658 - currY, 2) <= num) {
                station_now = "Shcholkovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1840) + "y:"
                        + Integer.toString(658);
                flag = true;
            } else if (Math.pow(1839 - currX, 2) + Math.pow(739 - currY, 2) <= num) {
                station_now = "Pervomayskaya";

                station_now_coor = " x:"
                        + Integer.toString(1839) + "y:"
                        + Integer.toString(739);
                flag = true;
            } else if (Math.pow(1839 - currX, 2) + Math.pow(779 - currY, 2) <= num) {
                station_now = "Izmaylovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1839) + "y:"
                        + Integer.toString(779);
                flag = true;
            } else if (Math.pow(1839 - currX, 2) + Math.pow(814 - currY, 2) <= num) {
                station_now = "Partizanskaya";

                station_now_coor = " x:"
                        + Integer.toString(1839) + "y:"
                        + Integer.toString(814);
                flag = true;
            } else if (Math.pow(1826 - currX, 2) + Math.pow(839 - currY, 2) <= num) {
                station_now = "Semyonovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1826) + "y:"
                        + Integer.toString(839);
                flag = true;
            } else if (Math.pow(1791 - currX, 2) + Math.pow(873 - currY, 2) <= num) {
                station_now = "Elektrozavodskaya";

                station_now_coor = " x:"
                        + Integer.toString(1791) + "y:"
                        + Integer.toString(873);
                flag = true;
            } else if (Math.pow(1748 - currX, 2) + Math.pow(918 - currY, 2) <= num) {
                station_now = "Baumanskaya";

                station_now_coor = " x:"
                        + Integer.toString(1748) + "y:"
                        + Integer.toString(918);
                flag = true;
            } else if (Math.pow(640 - currX, 2) + Math.pow(1067 - currY, 2) <= num) {
                station_now = "Kiyevskaya";

                station_now_coor = " x:"
                        + Integer.toString(640) + "y:"
                        + Integer.toString(1067);
                flag = true;
            } else if (Math.pow(760 - currX, 2) + Math.pow(1067 - currY, 2) <= num) {
                station_now = "Smolenskaya(A-P)";

                station_now_coor = " x:"
                        + Integer.toString(760) + "y:"
                        + Integer.toString(1067);
                flag = true;
            } else if (Math.pow(802 - currX, 2) + Math.pow(1006 - currY, 2) <= num) {
                station_now = "Arbatskaya(F)";

                station_now_coor = " x:"
                        + Integer.toString(802) + "y:"
                        + Integer.toString(1006);
                flag = true;
            } else if (Math.pow(717 - currX, 2) + Math.pow(1007 - currY, 2) <= num) {
                station_now = "Smolenskaya(F)";

                station_now_coor = " x:"
                        + Integer.toString(717) + "y:"
                        + Integer.toString(1007);
                flag = true;
            } else if (Math.pow(399 - currX, 2) + Math.pow(984 - currY, 2) <= num) {
                station_now = "Vystavochnaya, Delovoy Tsentr";

                station_now_coor = " x:"
                        + Integer.toString(399) + "y:"
                        + Integer.toString(984);
                flag = true;
            } else if (Math.pow(263 - currX, 2) + Math.pow(982 - currY, 2) <= num) {
                station_now = "Mezhdunarodnaya";

                station_now_coor = " x:"
                        + Integer.toString(263) + "y:"
                        + Integer.toString(982);
                flag = true;
            } else if (Math.pow(399 - currX, 2) + Math.pow(984 - currY, 2) <= num) {
                station_now = "Vystavochnaya(F), Delovoy Tsentr";

                station_now_coor = " x:"
                        + Integer.toString(399) + "y:"
                        + Integer.toString(984);
                flag = true;
            } else if (Math.pow(399 - currX, 2) + Math.pow(1147 - currY, 2) <= num) {
                station_now = "Park Pobedy";

                station_now_coor = " x:"
                        + Integer.toString(399) + "y:"
                        + Integer.toString(1147);
                flag = true;
            } else if (Math.pow(205 - currX, 2) + Math.pow(1147 - currY, 2) <= num) {
                station_now = "Slavyansky Bulvar";

                station_now_coor = " x:"
                        + Integer.toString(205) + "y:"
                        + Integer.toString(1147);
                flag = true;
            } else if (Math.pow(78 - currX, 2) + Math.pow(726 - currY, 2) <= num) {
                station_now = "Kuntsevskaya";

                station_now_coor = " x:"
                        + Integer.toString(78) + "y:"
                        + Integer.toString(726);
                flag = true;
            } else if (Math.pow(78 - currX, 2) + Math.pow(664 - currY, 2) <= num) {
                station_now = "Molodyozhnaya";

                station_now_coor = " x:"
                        + Integer.toString(78) + "y:"
                        + Integer.toString(664);
                flag = true;
            } else if (Math.pow(78 - currX, 2) + Math.pow(623 - currY, 2) <= num) {
                station_now = "Krylatskoye";

                station_now_coor = " x:"
                        + Integer.toString(78) + "y:"
                        + Integer.toString(623);
                flag = true;
            } else if (Math.pow(78 - currX, 2) + Math.pow(582 - currY, 2) <= num) {
                station_now = "Strogino";

                station_now_coor = " x:"
                        + Integer.toString(78) + "y:"
                        + Integer.toString(582);
                flag = true;
            } else if (Math.pow(78 - currX, 2) + Math.pow(545 - currY, 2) <= num) {
                station_now = "Myakinino";

                station_now_coor = " x:"
                        + Integer.toString(78) + "y:"
                        + Integer.toString(545);
                flag = true;
            } else if (Math.pow(78 - currX, 2) + Math.pow(502 - currY, 2) <= num) {
                station_now = "Volokamskaya";

                station_now_coor = " x:"
                        + Integer.toString(78) + "y:"
                        + Integer.toString(502);
                flag = true;
            } else if (Math.pow(78 - currX, 2) + Math.pow(462 - currY, 2) <= num) {
                station_now = "Mitino";

                station_now_coor = " x:"
                        + Integer.toString(78) + "y:"
                        + Integer.toString(462);
                flag = true;
            } else if (Math.pow(78 - currX, 2) + Math.pow(425 - currY, 2) <= num) {
                station_now = "Pyatnitskoye Shosse";

                station_now_coor = " x:"
                        + Integer.toString(78) + "y:"
                        + Integer.toString(425);
                flag = true;
            } else if (Math.pow(159 - currX, 2) + Math.pow(739 - currY, 2) <= num) {
                station_now = "Pionerskaya";

                station_now_coor = " x:"
                        + Integer.toString(159) + "y:"
                        + Integer.toString(739);
                flag = true;
            } else if (Math.pow(159 - currX, 2) + Math.pow(778 - currY, 2) <= num) {
                station_now = "Filyovsky Park";

                station_now_coor = " x:"
                        + Integer.toString(159) + "y:"
                        + Integer.toString(778);
                flag = true;
            } else if (Math.pow(159 - currX, 2) + Math.pow(818 - currY, 2) <= num) {
                station_now = "Bagrationovskaya";

                station_now_coor = " x:"
                        + Integer.toString(159) + "y:"
                        + Integer.toString(818);
                flag = true;
            } else if (Math.pow(159 - currX, 2) + Math.pow(858 - currY, 2) <= num) {
                station_now = "Fili";

                station_now_coor = " x:"
                        + Integer.toString(159) + "y:"
                        + Integer.toString(858);
                flag = true;
            } else if (Math.pow(159 - currX, 2) + Math.pow(899 - currY, 2) <= num) {
                station_now = "Kutuzovkskaya";

                station_now_coor = " x:"
                        + Integer.toString(159) + "y:"
                        + Integer.toString(899);
                flag = true;
            } else if (Math.pow(159 - currX, 2) + Math.pow(939 - currY, 2) <= num) {
                station_now = "Studencheskaya";

                station_now_coor = " x:"
                        + Integer.toString(159) + "y:"
                        + Integer.toString(939);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(1737 - currY, 2) <= num) {
                station_now = "Salaryevo";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(1737);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(1699 - currY, 2) <= num) {
                station_now = "Rumyantsevo";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(1699);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(1659 - currY, 2) <= num) {
                station_now = "Troparyovo";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(1659);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(1618 - currY, 2) <= num) {
                station_now = "Yugo - Zapadnaya";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(1618);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(1578 - currY, 2) <= num) {
                station_now = "Prospekt Vernandskogo";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(1578);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(1539 - currY, 2) <= num) {
                station_now = "Universitet";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(1539);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(1499 - currY, 2) <= num) {
                station_now = "Vorobyovy Gory";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(1499);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(1459 - currY, 2) <= num) {
                station_now = "Sportivnaya";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(1459);
                flag = true;
            } else if (Math.pow(639 - currX, 2) + Math.pow(1341 - currY, 2) <= num) {
                station_now = "Frunzenskaya";

                station_now_coor = " x:"
                        + Integer.toString(639) + "y:"
                        + Integer.toString(1341);
                flag = true;
            } else if (Math.pow(695 - currX, 2) + Math.pow(1249 - currY, 2) <= num) {
                station_now = "Park Kultury";

                station_now_coor = " x:"
                        + Integer.toString(695) + "y:"
                        + Integer.toString(1249);
                flag = true;
            } else if (Math.pow(1513 - currX, 2) + Math.pow(744 - currY, 2) <= num) {
                station_now = "Komsomolskaya";

                station_now_coor = " x:"
                        + Integer.toString(1513) + "y:"
                        + Integer.toString(744);
                flag = true;
            } else if (Math.pow(1584 - currX, 2) + Math.pow(679 - currY, 2) <= num) {
                station_now = "Krasnoselskaya";

                station_now_coor = " x:"
                        + Integer.toString(1584) + "y:"
                        + Integer.toString(679);
                flag = true;
            } else if (Math.pow(1597 - currX, 2) + Math.pow(656 - currY, 2) <= num) {
                station_now = "Sokolniki";

                station_now_coor = " x:"
                        + Integer.toString(1597) + "y:"
                        + Integer.toString(656);
                flag = true;
            } else if (Math.pow(1598 - currX, 2) + Math.pow(598 - currY, 2) <= num) {
                station_now = "Preobrazhenskaya Ploshchad";

                station_now_coor = " x:"
                        + Integer.toString(1598) + "y:"
                        + Integer.toString(598);
                flag = true;
            } else if (Math.pow(1598 - currX, 2) + Math.pow(558 - currY, 2) <= num) {
                station_now = "Cherkizovskaya";

                station_now_coor = " x:"
                        + Integer.toString(1598) + "y:"
                        + Integer.toString(558);
                flag = true;
            } else if (Math.pow(1598 - currX, 2) + Math.pow(520 - currY, 2) <= num) {
                station_now = "Bulvar Rokossovskogo";

                station_now_coor = " x:"
                        + Integer.toString(1598) + "y:"
                        + Integer.toString(520);
                flag = true;
            } else if (Math.pow(1379 - currX, 2) + Math.pow(746 - currY, 2) <= num) {
                station_now = "Krasniye Vorota";

                station_now_coor = " x:"
                        + Integer.toString(1379) + "y:"
                        + Integer.toString(746);
                flag = true;
            } else if (Math.pow(788 - currX, 2) + Math.pow(1156 - currY, 2) <= num) {
                station_now = "Kropotkinskaya";

                station_now_coor = " x:"
                        + Integer.toString(788) + "y:"
                        + Integer.toString(1156);
                flag = true;
            } else if (Math.pow(398 - currX, 2) + Math.pow(662 - currY, 2) <= num) {
                station_now = "Polezhaevskaya";
                station_now_coor = " x:"
                        + Integer.toString(398) + "y:"
                        + Integer.toString(662);
                flag = true;
            } else if (Math.pow(640 - currX, 2) + Math.pow(544 - currY, 2) <= num) {
                station_now = "Dinamo";
                station_now_coor = " x:"
                        + Integer.toString(640) + "y:"
                        + Integer.toString(544);
                flag = true;
            }
            Boolean flag_two = true;
            String[] parts = station_now.split(",");
            if(!(parts.length > 1 && parts[1] != "" || station_now == "Kashirskaya" ||
                    station_now == "Paveletskaya" || station_now == "Oktyabrskaya" || station_now == "Park Kultury"
                    || station_now == "Kitay - Gorod" || station_now == "Kiyevskaya" || station_now == "Park Pobedy"
                    || station_now == "Kuntsevskaya" || station_now == "Komsomolskaya" ||
                    station_now == "Belorusskaya" || station_now == "Prospekt Mira")){
                flag_two = false;
            }
            if(flag && flag_two){
                text.setText("Choose your station here!");
            } else if(flag && !flag_two){
                text.setText(station_now);
            }
        }
        return true;
    }

    public float[] imageCoord(ImageView v) {
        Matrix m = v.getImageMatrix();
        float[] value = new float[9];
        m.getValues(value);
        return value;
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        float out = (float) Math.sqrt(x * x + y * y);
        return out;
    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);
            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }

    public void registerForContextMenu(View view) {
        view.setOnCreateContextMenuListener(this);
    }
//choose station in change
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        switch (v.getId()) {
            case R.id.textView:
                switch (station_now) {
                    case "Prospekt Mira":
                        menu.add(Menu.NONE, MENU_PROSPEKTMIRA_ORANGE, Menu.NONE, "Prospekt Mira(Rizhskaya)");
                        menu.add(Menu.NONE, MENU_PROSPEKTMIRA_BROWN, Menu.NONE, "Prospekt Mira(Koltsevaya)");
                        break;
                    case "Chistiye Prudy, Turgenevskaya, Sretensky Bulvar":
                        menu.add(Menu.NONE, MENU_CHISTYE_PRUDY, Menu.NONE, "Chistiye Prudy");
                        menu.add(Menu.NONE, MENU_TURGENEVSKAYA, Menu.NONE, "Turgenevskaya");
                        menu.add(Menu.NONE, MENU_SRETENSKY_BULVAR, Menu.NONE, "Sretensky Bulvar");
                        break;
                    case "Kitay - Gorod":
                        menu.add(Menu.NONE, MENU_KITAY_GOROD_PURPLE, Menu.NONE, "Kitay - Gorod(Tagansko-Krasnopesnenskaya)");
                        menu.add(Menu.NONE, MENU_KITAY_GOROD_ORANGE, Menu.NONE, "Turgenevskaya(Rizhskaya)");
                        break;
                    case "Tretyakovskaya, Novokuznetskaya":
                        menu.add(Menu.NONE, MENU_TRETYAKOVSKAYA_YELLOW, Menu.NONE, "Tretyakovskaya(Kalininskaya)");
                        menu.add(Menu.NONE, MENU_TRETYAKOVSKAYA_ORANE, Menu.NONE, "Tretyakovskaya(Rizhskaya)");
                        menu.add(Menu.NONE, MENU_NOVOKUZNETSKAYA, Menu.NONE, "Novokuznetskaya");
                        break;
                    case "Oktyabrskaya":
                        menu.add(Menu.NONE, MENU_OKTABRSKAYA_ORANGE, Menu.NONE, "Oktyabrskaya(Rizhskaya)");
                        menu.add(Menu.NONE, MENU_OKTYABRSKAYA_BROWN, Menu.NONE, "Oktyabrskaya(Koltsevaya)");
                        break;
                    case "Bitsevsky Park, Novoyasenevskaya":
                        menu.add(Menu.NONE, MENU_NOVOYASENEVSKAYA, Menu.NONE, "Novoyasenevskaya");
                        menu.add(Menu.NONE, MENU_BITSEVSKY_PARK, Menu.NONE, "Bitsevsky Park");
                        break;
                    case "Bulvar Dmitriya Donskogo, Ulitsa Starokachalovskaya":
                        menu.add(Menu.NONE, MENU_BULVAR_DMITRIYA_DONSKOGO, Menu.NONE, "Bulvar Dmitriya Donskogo");
                        menu.add(Menu.NONE, MENU_ULITSA_STAROKACHALOVA, Menu.NONE, "Ulitsa Starokachalovskaya");
                        break;
                    case "Kakhovskaya, Sevastopolskaya":
                        menu.add(Menu.NONE, MENU_KAKHOVSKAYA, Menu.NONE, "Kakhovskaya");
                        menu.add(Menu.NONE, MENU_SEVASTOPOLSKAYA, Menu.NONE, "Sevastoplskaya");
                        break;
                    case "Dobryninskaya, Serpukhovskaya":
                        menu.add(Menu.NONE, MENU_DOBRYNINSKAYA, Menu.NONE, "Dobryninskaya");
                        menu.add(Menu.NONE, MENU_SERPUKHOVSKAYA, Menu.NONE, "Serpukhovskaya");
                        break;
                    case "Biblioteka Imeni Lenina, Aleksandrovsky Sad, Arbatskaya(A-P), Borovitskaya":
                        menu.add(Menu.NONE, MENU_BIBLIOTEKA_IMENI_LENINA, Menu.NONE, "Biblioteka Imeni Lenina");
                        menu.add(Menu.NONE, MENU_ALEKSANDROVSKY_SAD, Menu.NONE, "Aleksandrovsky Sad");
                        menu.add(Menu.NONE, MENU_ARBATSKAYA, Menu.NONE, "Arbatskaya(Arbatsko-Pokrovskaya)");
                        menu.add(Menu.NONE, MENU_BOROVITSKAYA, Menu.NONE, "Borovitskaya");
                        break;
                    case "Pushkinskaya, Chekhovskaya, Tverskaya":
                        menu.add(Menu.NONE, MENU_PUSHKINSKAYA, Menu.NONE, "Pushkinskaya");
                        menu.add(Menu.NONE, MENU_TVERSKAYA, Menu.NONE, "Tverskaya");
                        menu.add(Menu.NONE, MENU_CHEKHOVSKAYA, Menu.NONE, "Chekhovskaya");
                        break;
                    case "Trubnaya, Tsvetnoy Bulvar":
                        menu.add(Menu.NONE, MENU_TRUBNAYA, Menu.NONE, "Trubnaya");
                        menu.add(Menu.NONE, MENU_TSVETNOY_BULVAR, Menu.NONE, "Tsvetnoy Bulvar");
                        break;
                    case "Mendeleevskaya, Novoslobodskaya":
                        menu.add(Menu.NONE, MENU_MENDELEEVSKAYA, Menu.NONE, "Mendeleevskaya");
                        menu.add(Menu.NONE, MENU_NOVOSLOBODSKAYA, Menu.NONE, "Novoslobodskaya");
                        break;
                    case "Kuznetsky Most, Lubyanka":
                        menu.add(Menu.NONE, MENU_LUBYANKA, Menu.NONE, "Lubyanka");
                        menu.add(Menu.NONE, MENU_KUZNETSKY_MOST, Menu.NONE, "Kuznetsy Most");
                        break;
                    case "Teatralnaya, Okhotny Ryad, Ploshchad Revolyutsii":
                        menu.add(Menu.NONE, MENU_TEATRALNAYA, Menu.NONE, "Teatralnaya");
                        menu.add(Menu.NONE, MENU_OKHOTNY_RYAD, Menu.NONE, "Okhotny Ryad");
                        menu.add(Menu.NONE, MENU_PLOSHCHAD_REVOLUTSII, Menu.NONE, "Ploshchad Revolyutsii");
                        break;
                    case "Komsomolskaya":
                        menu.add(Menu.NONE, MENU_KOMSOMOLSKAYA_BROWN, Menu.NONE, "Komsomolskaya(Koltsevaya)");
                        menu.add(Menu.NONE, MENU_KOMSOMOLSKAYA_RED, Menu.NONE, "Komsomolskaya(Sokolnicheskaya)");
                        break;
                    case "Kurskaya, Chkalovskaya":
                        menu.add(Menu.NONE, MENU_KURSKAYA_BLUE, Menu.NONE, "Kurskaya(Arbatsko-Pokrovskaya)");
                        menu.add(Menu.NONE, MENU_KURSKAYA_BROWN, Menu.NONE, "Kurskaya(Koltsevaya)");
                        menu.add(Menu.NONE, MENU_CHKALOVSKAYA, Menu.NONE, "Chkalovskaya");
                        break;
                    case "Marksistskaya, Taganskaya":
                        menu.add(Menu.NONE, MENU_TAGANSKAYA_PURPLE, Menu.NONE, "Taganskaya(Tagansko-Krasnopresnenskaya)");
                        menu.add(Menu.NONE, MENU_TAGANSKAYA_BROWN, Menu.NONE, "Taganskaya(Koltsevaya)");
                        menu.add(Menu.NONE, MENU_MARKSISTSKAYA, Menu.NONE, "Marksistskaya");
                        break;
                    case "Paveletskaya":
                        menu.add(Menu.NONE, MENU_PAVELETSKAYA_BROWN, Menu.NONE, "Paveletskaya(Koltsevaya)");
                        menu.add(Menu.NONE, MENU_PAVELETSKAYA_GREEN, Menu.NONE, "Paveletskaya(Zamoskvoretskaya)");
                        break;
                    case "Kashirskaya":
                        menu.add(Menu.NONE, MENU_KASHIRSKAYA_BLUE_GREY, Menu.NONE, "Kashirskaya(Kakhovskaya)");
                        menu.add(Menu.NONE, MENU_KASHIRSKAYA_GREEN, Menu.NONE, "Kashirskaya(Zamoskvoretskaya)");
                        break;
                    case "Zyablikovo, Krasnogvardeyskaya":
                        menu.add(Menu.NONE, MENU_ZYABLOKOVO, Menu.NONE, "Zyablikovo");
                        menu.add(Menu.NONE, MENU_KRASNOGVARDEYSKAYA, Menu.NONE, "Krasnogvardeyskaya");
                        break;
                    case "Krestyanskaya Zastava, Proletarskaya":
                        menu.add(Menu.NONE, MENU_PROLETARSKAYA, Menu.NONE, "Proletarskaya");
                        menu.add(Menu.NONE, MENU_KRESTYANSKAYA_ZASTAVA, Menu.NONE, "Krestyanskaya Zastava");
                        break;
                    case "Rimskaya, Ploshchad Ilicha":
                        menu.add(Menu.NONE, MENU_RIMSKAYA, Menu.NONE, "Rimskaya");
                        menu.add(Menu.NONE, MENU_PLOSHCHAD_ILICHA, Menu.NONE, "Ploshchad Ilicha");
                        break;
                    case "Belorusskaya":
                        menu.add(Menu.NONE, MENU_BELORUSSKAYA_BROWN, Menu.NONE, "Belorusskaya(Koltsevaya)");
                        menu.add(Menu.NONE, MENU_BELORUSSKAYA_GREEN, Menu.NONE, "Belorusskaya(Zamoskvoretskaya)");
                        break;
                    case "Krasnopresnenskaya, Barrikadnaya":
                        menu.add(Menu.NONE, MENU_KRASNOPRESNENSKAYA, Menu.NONE, "Krasnopresnenskaya");
                        menu.add(Menu.NONE, MENU_BARRIKADNAYA, Menu.NONE, "Barrikadnaya");
                        break;
                    case "Kiyevskaya":
                        menu.add(Menu.NONE, MENU_KIYEVSKAYA_BLUE, Menu.NONE, "Kiyevskaya(Arbatsko-Pokrovskaya)");
                        menu.add(Menu.NONE, MENU_KIYEVSKAYA_BROWN, Menu.NONE, "Kiyevskaya(Koltsevaya)");
                        menu.add(Menu.NONE, MENU_KIYEVSKAYA_SKY, Menu.NONE, "Kiyevskaya(Filyovskaya)");
                        break;
                    case "Kuntsevskaya":
                        menu.add(Menu.NONE, MENU_KUNTSEVSKAYA_BLUE, Menu.NONE, "Kuntsevskaya(Arbatsko-Pokrovskaya)");
                        menu.add(Menu.NONE, MENU_KUNTSEVSKAYA_SKY, Menu.NONE, "Kuntsevskaya(Filyovskaya)");
                        break;
                    case "Park Pobedy":
                        menu.add(Menu.NONE, MENU_PARK_POBEDY_BLUE, Menu.NONE, "Park Pobedy(Arbatsko-Pokrovskaya)");
                        menu.add(Menu.NONE, MENU_PARK_POBEDY_YELLOW, Menu.NONE, "Park Pobedy(Kaliniskaya)");
                        break;
                    case "Vystavochnaya, Delovoy Tsentr":
                        menu.add(Menu.NONE, MENU_VYSTAVOCHNAYA, Menu.NONE, "Vystavochnaya");
                        menu.add(Menu.NONE, MENU_DELOVOY_TSENTR, Menu.NONE, "Delovoy Tsentr");
                        break;
                    case "Park Kultury":
                        menu.add(Menu.NONE, MENU_PARK_KULTURY_BROWN, Menu.NONE, "Park Kultury(Koltsevaya)");
                        menu.add(Menu.NONE, MENU_PARK_KULTURY_RED, Menu.NONE, "Park Kultury(Sokolnicheskaya)");
                        break;

                }
                break;
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_PROSPEKTMIRA_ORANGE:
                station_now = "Prospekt Mira(R)";
                break;
            case MENU_PROSPEKTMIRA_BROWN:
                station_now = "Prospekt Mira(K)";
                break;
            case MENU_CHISTYE_PRUDY:
                station_now = "Chistiye Prudy";
                break;
            case MENU_TURGENEVSKAYA:
                station_now = "Turgenevskaya";
                break;
            case MENU_SRETENSKY_BULVAR:
                station_now = "Sretensky Bulvar";
                break;
            case MENU_KITAY_GOROD_PURPLE:
                station_now = "Kitay - Gorod(T-K)";
                break;
            case MENU_KITAY_GOROD_ORANGE:
                station_now = "Kitay - Gorod(K-R)";
                break;
            case MENU_TRETYAKOVSKAYA_ORANE:
                station_now = "Tretyakovskaya(K-R)";
                break;
            case MENU_TRETYAKOVSKAYA_YELLOW:
                station_now = "Tretyakovskaya(K)";
                break;
            case MENU_NOVOKUZNETSKAYA:
                station_now = "Novokuznetskaya";
                break;
            case MENU_OKTYABRSKAYA_BROWN:
                station_now = "Oktyabrskaya(K)";
                break;
            case MENU_OKTABRSKAYA_ORANGE:
                station_now = "Oktyabrskaya(K-R)";
                break;
            case MENU_NOVOYASENEVSKAYA:
                station_now = "Novoyasenevskaya";
                break;
            case MENU_BITSEVSKY_PARK:
                station_now = "Bitsevsky Park";
                break;
            case MENU_BULVAR_DMITRIYA_DONSKOGO:
                station_now = "Bulvar Dmitriya Donskogo";
                break;
            case MENU_ULITSA_STAROKACHALOVA:
                station_now = "Ulitsa Starokachalovskaya";
                break;
            case MENU_KAKHOVSKAYA:
                station_now = "Kakhovskaya";
                break;
            case MENU_SEVASTOPOLSKAYA:
                station_now = "Sevastopolskaya";
                break;
            case MENU_SERPUKHOVSKAYA:
                station_now = "Serpukhovskaya";
                break;
            case MENU_DOBRYNINSKAYA:
                station_now = "Dobryninskaya";
                break;
            case MENU_BIBLIOTEKA_IMENI_LENINA:
                station_now = "Biblioteka Imeni Lenina";
                break;
            case MENU_ALEKSANDROVSKY_SAD:
                station_now = "Aleksandrovsky Sad";
                break;
            case MENU_ARBATSKAYA:
                station_now = "Arbatskaya(A-P)";
                break;
            case MENU_BOROVITSKAYA:
                station_now = "Borovitskaya";
                break;
            case MENU_TRUBNAYA:
                station_now = "Trubnaya";
                break;
            case MENU_TSVETNOY_BULVAR:
                station_now = "Tsvetnoy Bulvar";
                break;
            case MENU_NOVOSLOBODSKAYA:
                station_now = "Novoslobodskaya";
                break;
            case MENU_MENDELEEVSKAYA:
                station_now = "Mendeleevskaya";
                break;
            case MENU_KUZNETSKY_MOST:
                station_now = "Kuznetsky Most";
                break;
            case MENU_LUBYANKA:
                station_now = "Lubyanka";
                break;
            case MENU_OKHOTNY_RYAD:
                station_now = "Okhotny Ryad";
                break;
            case MENU_TEATRALNAYA:
                station_now = "Teatralnaya";
                break;
            case MENU_PLOSHCHAD_REVOLUTSII:
                station_now = "Ploshchad Revolyutsii";
                break;
            case MENU_KOMSOMOLSKAYA_BROWN:
                station_now = "Komsomolskaya(K)";
                break;
            case MENU_KOMSOMOLSKAYA_RED:
                station_now = "Komsomolskaya(S)";
                break;
            case MENU_KURSKAYA_BLUE:
                station_now = "Kurskaya(A-P)";
                break;
            case MENU_KURSKAYA_BROWN:
                station_now = "Kurskaya(K)";
                break;
            case MENU_CHKALOVSKAYA:
                station_now = "Chkalovskaya";
                break;
            case MENU_MARKSISTSKAYA:
                station_now = "Marksistskaya";
                break;
            case MENU_TAGANSKAYA_BROWN:
                station_now = "Taganskaya(K)";
                break;
            case MENU_TAGANSKAYA_PURPLE:
                station_now = "Taganskaya(T-K)";
                break;
            case MENU_PAVELETSKAYA_BROWN:
                station_now = "Paveletskaya(K)";
                break;
            case MENU_PAVELETSKAYA_GREEN:
                station_now = "Paveletskaya(Z)";
                break;
            case MENU_KASHIRSKAYA_BLUE_GREY:
                station_now = "Kashirskaya(Kakh)";
                break;
            case MENU_KASHIRSKAYA_GREEN:
                station_now = "Kashirskaya(Z)";
                break;
            case MENU_ZYABLOKOVO:
                station_now = "Zyablikovo";
                break;
            case MENU_KRASNOGVARDEYSKAYA:
                station_now = "Krasnogvardeyskaya";
                break;
            case MENU_KRESTYANSKAYA_ZASTAVA:
                station_now = "Krestyanskaya Zastava";
                break;
            case MENU_PROLETARSKAYA:
                station_now = "Proletarskaya";
                break;
            case MENU_PLOSHCHAD_ILICHA:
                station_now = "Ploshchad Ilicha";
                break;
            case MENU_RIMSKAYA:
                station_now = "Rimskaya";
                break;
            case MENU_BELORUSSKAYA_BROWN:
                station_now = "Belorusskaya(K)";
                break;
            case MENU_BELORUSSKAYA_GREEN:
                station_now = "Belorusskaya(Z)";
                break;
            case MENU_BARRIKADNAYA:
                station_now = "Barrikadnaya";
                break;
            case MENU_KRASNOPRESNENSKAYA:
                station_now = "Krasnopresnenskaya";
                break;
            case MENU_KIYEVSKAYA_BLUE:
                station_now = "Kiyevskaya(A-P)";
                break;
            case MENU_KIYEVSKAYA_BROWN:
                station_now = "Kiyevskaya(K)";
                break;
            case MENU_KIYEVSKAYA_SKY:
                station_now = "Kiyevskaya(F)";
                break;
            case MENU_KUNTSEVSKAYA_BLUE:
                station_now = "Kuntsevskaya(A-P)";
                break;
            case MENU_KUNTSEVSKAYA_SKY:
                station_now = "Kuntsevskaya(F)";
                break;
            case MENU_PARK_POBEDY_BLUE:
                station_now = "Park Pobedy(A-P)";
                break;
            case MENU_PARK_POBEDY_YELLOW:
                station_now = "Park Pobedy(K)";
                break;
            case MENU_DELOVOY_TSENTR:
                station_now = "Delovoy Tsentr";
                break;
            case MENU_VYSTAVOCHNAYA:
                station_now = "Vystavochnaya";
                break;
            case MENU_PUSHKINSKAYA:
                station_now = "Pushkinskaya";
                break;
            case MENU_TVERSKAYA:
                station_now = "Tverskaya";
                break;
            case MENU_CHEKHOVSKAYA:
                station_now = "Chekhovskaya";
                break;
            case MENU_PARK_KULTURY_BROWN:
                station_now = "Park Kultury(K)";
                break;
            case MENU_PARK_KULTURY_RED:
                station_now = "Park Kultury(R)";
                break;
        }
        text.setText(station_now);
        return super.onContextItemSelected(item);
    }
//read file from internal storage
    public String readFile(String fileName) {
        String out = "";
        try {
            BufferedReader inputReader = new BufferedReader
                    (new InputStreamReader(openFileInput(fileName)));
            String inputString;
            StringBuffer stringBuffer = new StringBuffer();
            while ((inputString = inputReader.readLine()) != null) {
                stringBuffer.append(inputString + "\n");
            }
            out = stringBuffer.toString();
            inputReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }
    //read file from external storage
    private String readFileSD(String filename) {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            File fb = new File(getFilesDir(), filename);
            if(fb.exists()){
                return readFile(filename);
            } else{
                return "";
            }
        } else{
            File sdPath = Environment.getExternalStorageDirectory();
            sdPath = new File(sdPath.getAbsolutePath() + "/" + filename);
            sdPath.mkdirs();
            File sdFile = new File(sdPath, filename);
            try {
                BufferedReader br = new BufferedReader(new FileReader(sdFile));
                String str = "";
                String output = "";
                while ((str = br.readLine()) != null) {
                    output += str;
                }
                return output;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return "";
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }
    }
    //write file to internal storage
    public void writeFile(String fileName, String str_wrt) {
        try {
            str_wrt += "\n";
            FileOutputStream outputStream = openFileOutput(fileName, Context.MODE_APPEND);
            outputStream.write(str_wrt.getBytes());
            outputStream.close();
            Toast toast = Toast.makeText(getApplicationContext(),
                    "OK", Toast.LENGTH_SHORT);
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//write file to external storage
    void writeFileSD(String text, String filename) {
        File sdPath = Environment.getExternalStorageDirectory();
        sdPath = new File(sdPath.getAbsolutePath() + "/" + filename);
        sdPath.mkdirs();
        File sdFile = new File(sdPath, filename);
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            writeFile(filename, text);
        } else {
            try {
                if (!sdFile.exists()){
                    sdFile.createNewFile();
                }
                FileWriter wrt = new FileWriter(sdFile, true);
                try {
                    wrt.append(text + "\n");
                } finally {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "OK", Toast.LENGTH_SHORT);
                    toast.show();
                    wrt.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
//check cellid and write it
    void onCellLocationChanged(String fileName) {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ){
            ActivityCompat.requestPermissions(MainScreen.this,
                    new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION},
                    10);
        } else {
            TelephonyManager telephonyManager = (TelephonyManager)
                    getSystemService(android.content.Context.TELEPHONY_SERVICE);
            SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            String dateString = time.format(date);
            if (telephonyManager.getCellLocation() instanceof GsmCellLocation) {
                GsmCellLocation gsmCell = (GsmCellLocation) telephonyManager.getCellLocation();
                String cid = Integer.toString(gsmCell.getCid());
                String lac = Integer.toString(gsmCell.getLac());
                String operator = telephonyManager.getNetworkOperator();
                String mcc = operator.substring(0, 3);
                String mnc = operator.substring(3);
                writeFileSD(dateString + " " + station_now + station_now_coor
                        + " " + dateString + " " + "id:" + mcc + ":" + mnc + ":" + lac + ":" + cid, file_name);
            } else {
                writeFileSD(station_now + " " + station_now_coor
                        + " " + dateString + "_" + "no signal", file_name);
            }
        }
    }
//log of touch
    private void dumpEvent(MotionEvent event) {
        String names[] = {"DOWN", "UP", "MOVE",
                "CANCEL", "OUTSIDE", "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"};
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);

        if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }

        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }

        sb.append("]");
        Log.d("Touch Events ---------", sb.toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}