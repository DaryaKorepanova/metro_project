package com.example.korep_000.metro3;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.util.Log;


public class Receiver extends BroadcastReceiver {

    final String LOG_TAG = "myLogs";


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(LOG_TAG, "onReceive");
        Intent serviceIntent = new Intent(context, Service.class);
        context.startService(serviceIntent);
    }
}